import {FrameEmitter} from "./logic/frameEmitter";
import {FrameCalculator} from "./logic/frameCalculator";
import {Scene} from "./logic/scene/scene";
import {SceneRenderer} from "./webgl/renderers/sceneRenderer";
import {Cube} from "./webgl/shapes/cube";
import {Camera} from "./logic/scene/camera";

const TARGET_FPS = 60;

const scene = new Scene(new Camera());
scene.camera.position.x = -3;

scene.rootEntity.shape = new Cube();

const renderer = new SceneRenderer(scene);
const frameCalculator = new FrameCalculator(eachSecond)
const frameEmitter = new FrameEmitter(render, TARGET_FPS);

function update() {
    frameCalculator.onFrameRendered();

    scene.rootEntity.rotation.x += 1 / 120;
    scene.rootEntity.rotation.y += 2 / 120;
    scene.rootEntity.rotation.z += 3 / 120;
}

function render() {
    update();

    renderer.clearScreen();
    renderer.renderFrame();
}

function eachSecond(fps) {
    document.title = `Kudze's Demo FPS: ${fps}`;
}