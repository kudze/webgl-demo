import VertexShaderCode from "../shaders/glsl/vertexEntity.glsl";
import FragmentShaderCode from "../shaders/glsl/fragmentEntity.glsl";
import { VertexFragmentRenderer } from "./vertexFragmentRenderer";
import {getContext} from "../context";

const gl = getContext();

export class EntityRenderer extends VertexFragmentRenderer {
    constructor(clearScreenColor = undefined) {
        super(VertexShaderCode, FragmentShaderCode, clearScreenColor);
    }

    renderEntity(entity, cameraMatrix, perspectiveMatrix) {
        if(entity.shape === null || entity.shape === undefined)
            return;

        entity.shape.vertexBuffer.bind();
        entity.shape.indexBuffer.bind();

        const cordLocation = gl.getAttribLocation(this.programHandle, "a_pos");
        const matrixLocation = gl.getUniformLocation(this.programHandle, "u_matrix");

        gl.vertexAttribPointer(cordLocation, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(cordLocation);

        const matrix = entity.computeMatrix();
        const rmatrix = perspectiveMatrix['*'](cameraMatrix['*'](matrix));

        gl.uniformMatrix4fv(matrixLocation, false, rmatrix.elements);
        gl.drawElements(gl.TRIANGLES, entity.shape.indexCount, gl.UNSIGNED_SHORT, 0);
    }

}