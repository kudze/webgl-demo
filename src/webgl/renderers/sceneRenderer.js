import {EntityRenderer} from "./entityRenderer";

export class SceneRenderer extends EntityRenderer
{
    scene;

    constructor(scene, clearScreenColor = undefined) {
        super(clearScreenColor);

        this.scene = scene;
    }

    renderFrame() {
        const cameraMatrix = this.scene.camera.initCameraMatrix();
        const perspectiveMatrix = this.scene.camera.initPerspectiveMatrix();

        this.renderEntity(this.scene.rootEntity, cameraMatrix, perspectiveMatrix);
    }
}