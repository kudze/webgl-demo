import {RGBAColor} from "../../util/color";
import {getContext} from "../context";

const gl = getContext();

export class Renderer {
    clearScreenColor = new RGBAColor(0, 0, 0);

    constructor(clearScreenColor = undefined) {
        if (clearScreenColor !== undefined) this.clearScreenColor = clearScreenColor;
    }

    clearScreen() {
        gl.clearColor(
            this.clearScreenColor.r,
            this.clearScreenColor.g,
            this.clearScreenColor.b,
            this.clearScreenColor.a
        );

        gl.clear(gl.COLOR_BUFFER_BIT);
    }

}