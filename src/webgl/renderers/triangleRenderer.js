import VertexShaderCode from "../shaders/glsl/vertexShape.glsl";
import FragmentShaderCode from "../shaders/glsl/fragmentShape.glsl";
import { VertexFragmentRenderer } from "./vertexFragmentRenderer";
import {getContext} from "../context";

const gl = getContext();


export class TriangleRenderer extends VertexFragmentRenderer {

    constructor(clearScreenColor = undefined) {
        super(VertexShaderCode, FragmentShaderCode, clearScreenColor);
    }

    renderShape(shape) {
        shape.vertexBuffer.bind();
        shape.indexBuffer.bind();

        const cord = gl.getAttribLocation(this.programHandle, "a_pos");
        gl.vertexAttribPointer(cord, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(cord);

        gl.drawElements(gl.TRIANGLES, shape.indexCount, gl.UNSIGNED_SHORT, 0);
    }
}