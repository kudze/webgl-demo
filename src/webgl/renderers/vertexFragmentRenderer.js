import {Renderer} from "./renderer";
import {VertexShader} from "../shaders/vertexShader";
import {FragmentShader} from "../shaders/fragmentShader";
import {getContext} from "../context";

const gl = getContext();

export class VertexFragmentRenderer extends Renderer {
    vertexShader;
    fragmentShader;
    programHandle;

    constructor(vertexShaderCode, fragmentShaderCode, clearScreenColor = undefined) {
        super(clearScreenColor);

        this.vertexShader = new VertexShader(vertexShaderCode)
        this.fragmentShader = new FragmentShader(fragmentShaderCode)

        this.programHandle = gl.createProgram();
        gl.attachShader(this.programHandle, this.vertexShader.handle);
        gl.attachShader(this.programHandle, this.fragmentShader.handle);
        gl.linkProgram(this.programHandle);
        gl.useProgram(this.programHandle); //TODO: maybe this needs be elsewhere.
    }

    clearScreen() {
        super.clearScreen();

        gl.enable(gl.DEPTH_TEST);
    }
}