import {getContext} from "../context";

const gl = getContext();

export class Shader {
    handle;

    constructor(code, shaderType) {
        this.handle = gl.createShader(shaderType);
        gl.shaderSource(this.handle, code);
        gl.compileShader(this.handle);
    }

}