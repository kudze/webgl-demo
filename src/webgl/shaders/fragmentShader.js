import {getContext} from "../context";
import {Shader} from "./shader";

const gl = getContext();

export class FragmentShader extends Shader {
    constructor(code) {
        super(code, gl.FRAGMENT_SHADER);
    }
}