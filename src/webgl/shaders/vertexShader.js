import {getContext} from "../context";
import {Shader} from "./shader";

const gl = getContext();

export class VertexShader extends Shader {
    constructor(code) {
        super(code, gl.VERTEX_SHADER);
    }
}