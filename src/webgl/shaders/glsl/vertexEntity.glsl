attribute vec3 a_pos;

uniform mat4 u_matrix;

void main(void) {
    gl_Position = u_matrix * vec4(a_pos, 1.0);
}