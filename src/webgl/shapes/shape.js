import {getContext} from "../context";
import {ArrayBuffer} from "../buffers/arrayBuffer";
import {ElementArrayBuffer} from "../buffers/elementArrayBuffer";

const gl = getContext();

export class Shape {
    vertexBuffer;
    indexBuffer;
    indexCount;

    constructor(vertices, indices) {
        this.vertexBuffer = new ArrayBuffer(new Float32Array(vertices));
        this.indexBuffer = new ElementArrayBuffer(new Uint16Array(indices));

        this.indexCount = indices.length;
    }
}