import {Shape} from "./shape";

const CUBE_VERTICES = [
    -1, -1, -1,
     1, -1, -1,
     1,  1, -1,
    -1,  1, -1,
    -1, -1,  1,
     1, -1,  1,
     1,  1,  1,
    -1,  1,  1
];

const CUBE_INDICES = [
    0, 1, 3, 3, 1, 2,
    1, 5, 2, 2, 5, 6,
    5, 4, 6, 6, 4, 7,
    4, 0, 7, 7, 0, 3,
    3, 2, 7, 7, 2, 6,
    4, 5, 0, 0, 5, 1
];

export class Cube extends Shape {

    constructor() {
        super(
            CUBE_VERTICES,
            CUBE_INDICES
        );
    }

}