import {Shape} from "./shape";

const TRIANGLE_VERTICES = [
    -0.5, 0.5, 0.0,
    -0.5, -0.5, 0.0,
    0.5, -0.5, 0.0,
];

const TRIANGLE_INDICES = [
    0, 1, 2
];

export class Triangle extends Shape {

    constructor() {
        super(
            TRIANGLE_VERTICES,
            TRIANGLE_INDICES
        );
    }

}