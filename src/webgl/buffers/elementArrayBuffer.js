import {Buffer} from "./buffer";
import {getContext} from "../context";

const gl = getContext();
const bufferType = gl.ELEMENT_ARRAY_BUFFER;

export class ElementArrayBuffer extends Buffer {

    constructor(data, bufferUsage = gl.STATIC_DRAW) {
        super(data, bufferType, bufferUsage);
    }

    loadBufferData(data, bufferUsage = gl.STATIC_DRAW) {
        return super.loadBufferData(data, bufferUsage, bufferType);
    }

    loadBufferDataBound(data, bufferUsage = gl.STATIC_DRAW) {
        return super.loadBufferDataBound(data, bufferUsage, bufferType);
    }

    bind() {
        return super.bind(bufferType);
    }

    unbind() {
        return super.unbind(bufferType);
    }

}