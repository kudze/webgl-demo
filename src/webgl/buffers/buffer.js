/**
 * Rn this really only just array buffer.
 */
import {getContext} from "../context";

const gl = getContext();

export class Buffer {
    handle = null;

    constructor(data, bufferType, bufferUsage = gl.STATIC_DRAW)
    {
        this.handle = gl.createBuffer();

        this.loadBufferData(data, bufferUsage, bufferType);
    }

    /**
     * Loads data into buffer, cause unbinding of said bufferType
     */
    loadBufferData(data, bufferUsage, bufferType) {
        this.bind(bufferType);
        this.loadBufferDataBound(data, bufferUsage, bufferType);
        this.unbind(bufferType);
    }

    /**
     * Loads data into buffer, which is already bound.
     */
    loadBufferDataBound(data, bufferUsage, bufferType) {
        gl.bufferData(bufferType, data, bufferUsage);
    }

    /**
     * Binds the buffer.
     */
    bind(bufferType) {
        gl.bindBuffer(bufferType, this.handle);
    }

    /**
     * Unbinds the buffer.
     */
    unbind(bufferType) {
        gl.bindBuffer(bufferType, null);
    }
}