import {getCanvas} from "./canvas";

const context = getCanvas().getContext('webgl');

export function getContext() {
    return context;
}
