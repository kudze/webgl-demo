/**
 * Emits frames for given FPS.
 */
export class FrameEmitter {
    callback;
    lastRequestID;

    constructor(callback) {
        this.callback = callback;

        this.lastRequestID = requestAnimationFrame(this.animFrame.bind(this));
    }

    stop() {
        cancelAnimationFrame(this.lastRequestID);
    }

    /**
     * @internal
     */
    animFrame() {
        this.callback();

        this.lastRequestID = requestAnimationFrame(this.animFrame.bind(this));
    }
}