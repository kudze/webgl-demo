/**
 * Calulates frames for fps.
 * executes callback each second giving back how much frames were put.
 */
export class FrameCalculator {
    callback = null;
    counter = 0;
    interval = null;

    constructor(callback) {
        this.callback = callback;
        this.interval = setInterval(
            this.eachSecond.bind(this),
            1000
        );
    }

    dispose() {
        if (this.interval === null)
            return;

        clearInterval(this.interval);
    }

    onFrameRendered() {
        this.counter++;
    }

    /**
     * @internal
     */
    eachSecond() {
        if (this.callback !== null && this.callback !== undefined)
            this.callback(this.counter);

        this.counter = 0;
    }
}