import "glm-js";
import {getCanvas} from "../../webgl/canvas";

const canvas = getCanvas();

export class Camera {
    position;
    lookAt;
    fov;

    constructor(position = glm.vec3(), lookAt = glm.vec3(1, 0, 0), fov = 90) {
        this.position = position;
        this.lookAt = lookAt;
        this.fov = fov;
    }

    initCameraMatrix() {
        return glm.lookAt(
            this.position,
            this.position['+'](this.lookAt['*'](0.01)),
            glm.vec3(0, 0, 1)
        );
    }

    initPerspectiveMatrix() {
        const { width, height } = canvas.getBoundingClientRect();
        const aspectRation = width / height;

        return glm.perspective(
            glm._radians(this.fov),
            aspectRation,
            0.01,
            1000.0
        );
    }
}