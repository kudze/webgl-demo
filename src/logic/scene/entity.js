import "glm-js";

export class Entity {
    position;
    rotation;
    scale;
    shape;
    children = [];

    constructor(position = glm.vec3(), rotation = glm.vec3(), scale = glm.vec3(1, 1, 1)) {
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }

    computeMatrix() {
        let position = glm.mat4();
        let rotation = glm.mat4();
        let scale = glm.mat4();

        scale = glm.scale(scale, this.scale);
        rotation = glm.rotate(rotation, this.rotation.x, glm.vec3(1, 0, 0));
        rotation = glm.rotate(rotation, this.rotation.y, glm.vec3(0, 1, 0));
        rotation = glm.rotate(rotation, this.rotation.z, glm.vec3(0, 0, 1));
        position = glm.translate(position, this.position);

        return position['*'](rotation['*'](scale));
    }
}