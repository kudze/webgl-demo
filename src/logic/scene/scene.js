import {Entity} from "./entity";


export class Scene {
    rootEntity = new Entity();
    camera;

    constructor(camera) {
        this.camera = camera;
    }
}