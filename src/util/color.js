export class RGBAColor {
    r = 0;
    g = 0;
    b = 0;
    a = 0;

    constructor(r, g, b, a = 1.0) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    };
}