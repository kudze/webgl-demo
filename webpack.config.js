const path = require('path');

module.exports = {
    mode: "development", //TODO: << Later change to production :D
    entry: './src/index.js',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'public/js'),
    },
    module: {
        rules: [
            {
                test: /\.glsl$/i,
                type: 'asset/source',
            }
        ]
    }
};